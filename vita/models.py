from django.db import models


class Saga(models.Model):

    titulo_saga = models.CharField(max_length=200)

    def __str__(self):
        return self.titulo_saga


class Tipo(models.Model):

    nombre_tipo = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre_tipo


class Juego(models.Model):

    titulo = models.CharField(max_length=200)
    terminado = models.BooleanField(default=False)
    siguiente_a_jugar = models.BooleanField(default=False)
    tiempo_en_terminar = models.IntegerField(default=0)
    portada = models.URLField(blank=True, null=True)
    notas = models.TextField(blank=True, null=True)

    saga = models.ForeignKey(Saga, on_delete=models.DO_NOTHING, blank=True, null=True)
    tipo = models.ManyToManyField(Tipo)

    def __str__(self):
        return self.titulo

    def get_tipos(self):
        return ", ".join([t.nombre_tipo for t in self.tipo.all()])
