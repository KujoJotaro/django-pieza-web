from django.contrib import admin
from django.utils.html import format_html

from .models import Saga, Tipo, Juego


class GameAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Datos Basicos', {'fields': ['titulo', 'saga', 'tiempo_en_terminar', 'terminado', 'tipo']}),
        ('Otros', {'fields': ['notas', 'siguiente_a_jugar', 'portada']})
    ]
    list_display = ('retitle', 'saga', 'time_hrs', 'recompleted', 'rewish_to_play', 'show_url', 'retipos')
    search_fields = ['titulo']
    list_per_page = 10
    list_filter = ('terminado', 'tiempo_en_terminar', 'siguiente_a_jugar')

    def show_url(self, obj):
        return format_html('<a href="%s"><img src="%s" height="150" width="100"/>' % (obj.portada, obj.portada))
    show_url.allow_tags = True
    show_url.short_description = "Portada"

    def time_hrs(self, obj):
        if obj.tiempo_en_terminar > 0:
            return "%s Hrs." % obj.tiempo_en_terminar
        else:
            return '-'

    def recompleted(self, obj):
        return 'Si' if obj.terminado else 'No'

    def rewish_to_play(self, obj):
        return 'Si' if obj.siguiente_a_jugar else 'No'

    def retitle(self, obj):
        return obj.titulo

    def retipos(self, obj):
        return obj.get_tipos()

    recompleted.short_description = 'Terminado'
    rewish_to_play.short_description = 'Siguiente'
    retitle.short_description = 'Juego'
    time_hrs.short_description = "Tiempo en completar"
    retipos.short_description = 'Tipo juego'


admin.site.register(Saga)
admin.site.register(Tipo)
admin.site.register(Juego, GameAdmin)