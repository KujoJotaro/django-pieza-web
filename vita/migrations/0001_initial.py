# Generated by Django 2.1.7 on 2019-03-01 21:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Juego',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(max_length=200)),
                ('terminado', models.BooleanField(default=False)),
                ('siguiente_a_jugar', models.BooleanField(default=False)),
                ('tiempo_en_terminar', models.IntegerField(default=0)),
                ('portada', models.URLField(blank=True, null=True)),
                ('notas', models.TextField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Saga',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo_saga', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Tipo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre_tipo', models.CharField(max_length=200)),
            ],
        ),
        migrations.AddField(
            model_name='juego',
            name='saga',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='vita.Saga'),
        ),
        migrations.AddField(
            model_name='juego',
            name='tipo',
            field=models.ManyToManyField(to='vita.Tipo'),
        ),
    ]
