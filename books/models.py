from django.db import models


class Autor(models.Model):

    nombre = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre


class Tipo(models.Model):

    nombre_tipo = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre_tipo


class Saga(models.Model):

    titulo_saga = models.CharField(max_length=200)

    def __str__(self):
        return self.titulo_saga


class Libro(models.Model):

    autor = models.ForeignKey(Autor, on_delete=models.DO_NOTHING)
    tipo = models.ForeignKey(Tipo, on_delete=models.DO_NOTHING)
    saga = models.ForeignKey(Saga, on_delete=models.DO_NOTHING, blank=True, null=True)

    titulo_libro = models.CharField(max_length=200)
    leido = models.BooleanField(default=True)
    notas = models.TextField(blank=True, null=True)
    portada = models.URLField(max_length=200, blank=True, null=True)

    def __str__(self):
        return self.titulo_libro
    
