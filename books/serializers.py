from rest_framework import serializers
from .models import Libro, Autor, Saga, Tipo


class AutorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Autor
        fields = ('id', 'nombre')


class SagaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Saga
        fields = ('id', 'titulo_saga')


class TipoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tipo
        fields = ('id', 'nombre_tipo')


class LibroSerializer(serializers.ModelSerializer):

    autor = AutorSerializer()
    tipo = TipoSerializer()
    saga = SagaSerializer()

    class Meta:
        model = Libro
        fields = ('id', 'autor', 'tipo', 'saga', 'titulo_libro', 'leido', 'notas', 'portada')

