from django.contrib import admin
from import_export import resources
from django.utils.html import format_html
from import_export.admin import ImportExportModelAdmin

from .models import Autor, Saga, Tipo, Libro


# Importar Exportar

class LibroResource(resources.ModelResource):
    class Meta:
        model = Libro


class AutorResource(resources.ModelResource):
    class Meta:
        model = Autor


class SagaResource(resources.ModelResource):
    class Meta:
        model = Saga


class TipoResource(resources.ModelResource):
    class Meta:
        model = Tipo


class BookAdmin(ImportExportModelAdmin):
    resource_class = LibroResource

    # Campos para Agregar

    fieldsets = [
        ('Datos Basicos', {'fields': ['titulo_libro', 'autor', 'tipo', 'saga']}),
        ('Otros', {'fields': ['notas', 'leido', 'portada']})
    ]

    # Campos de lista

    list_display = ('titulo_libro', 'autor', 'tipo', 'saga', 'releido', 'show_url')
    list_filter = ('leido', 'tipo')
    search_fields = ['titulo_libro', 'autor__nombre']
    list_per_page = 10

    def releido(self, obj):
        return 'Si' if obj.leido else 'No'

    # Mostrar imagen

    def show_url(self, obj):
        return format_html('<a href="%s"><img src="%s" height="150" width="100"/>' % (obj.portada, obj.portada))

    show_url.allow_tags = True
    show_url.short_description = "Portada"

    releido.short_description = "Leido"


class AutorAdmin(ImportExportModelAdmin):
    resource_class = AutorResource


class TipoAdmin(ImportExportModelAdmin):
    resource_class = TipoResource
    search_fields = ['nombre_tipo']


class SagaAdmin(ImportExportModelAdmin):
    resource_class = SagaResource
    search_fields = ['titulo_saga']


admin.site.register(Autor, AutorAdmin)
admin.site.register(Saga, SagaAdmin)
admin.site.register(Tipo, TipoAdmin)
admin.site.register(Libro, BookAdmin)
