from django.shortcuts import render
from django.http import HttpResponse
from .models import Libro, Autor, Saga, Tipo
from rest_framework import viewsets, filters
from .serializers import AutorSerializer, TipoSerializer, SagaSerializer, LibroSerializer


class AutorViewSet(viewsets.ModelViewSet):
    queryset = Autor.objects.all()
    serializer_class = AutorSerializer


class LibroViewSet(viewsets.ModelViewSet):
    queryset = Libro.objects.all()
    serializer_class = LibroSerializer
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('titulo_libro', 'saga', 'autor', 'tipo')


class SagaViewSet(viewsets.ModelViewSet):
    queryset = Saga.objects.all()
    serializer_class = SagaSerializer


class TipoViewSet(viewsets.ModelViewSet):
    queryset = Tipo.objects.all()
    serializer_class = TipoSerializer