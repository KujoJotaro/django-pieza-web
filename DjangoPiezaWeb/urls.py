from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from books import views

routers = routers.DefaultRouter()
routers.register(r'libros', views.LibroViewSet)
routers.register(r'tipos', views.TipoViewSet)
routers.register(r'autores', views.AutorViewSet)
routers.register(r'sagas', views.SagaViewSet)

urlpatterns = [
    path('admin/',  admin.site.urls),
    path('api/', include(routers.urls))
]
